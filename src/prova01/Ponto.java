package prova01;

public class Ponto {
	private double x;
	private double y;

	public Ponto(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double dist(Ponto ponto) {
		return Math.sqrt(Math.pow(x - ponto.x, 2) + Math.pow(y - ponto.y, 2));
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
