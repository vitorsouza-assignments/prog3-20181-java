package prova01;

public interface Forma {
	double calcularPerimetro();

	double calcularArea();
}
