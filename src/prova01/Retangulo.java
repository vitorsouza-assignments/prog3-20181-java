package prova01;

public class Retangulo extends Poligono {
	private Ponto inicio;
	private double lado01;
	private double lado02;

	public Retangulo(Ponto p1, Ponto p2, Ponto p3, Ponto p4) {
		super(new double[] { p1.dist(p2), p2.dist(p3), p3.dist(p4), p4.dist(p1) });
		inicio = p1;
		lado01 = p1.dist(p2);
		lado02 = p2.dist(p3);
	}

	@Override
	public double calcularArea() {
		return lado01 * lado02;
	}

	@Override
	public String toString() {
		return "O Retângulo que começa em " + inicio + " tem perímetro = " + calcularPerimetro() + " e área = " + calcularArea();
	}
}
