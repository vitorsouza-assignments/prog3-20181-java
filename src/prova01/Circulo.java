package prova01;

public class Circulo implements Forma {
	private Ponto centro;
	private double raio;

	public Circulo(Ponto centro, double raio) {
		this.centro = centro;
		this.raio = raio;
	}

	@Override
	public double calcularPerimetro() {
		return 2 * Math.PI * raio;
	}

	@Override
	public double calcularArea() {
		return Math.PI * raio * raio;
	}

	@Override
	public String toString() {
		return "O Círculo com centro em " + centro + " tem perímetro = " + calcularPerimetro() + " e área = " + calcularArea();
	}
}
