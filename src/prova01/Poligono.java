package prova01;

public abstract class Poligono implements Forma {
	private double[] tamanhos;

	protected Poligono(double[] tamanhos) {
		this.tamanhos = tamanhos;
	}

	@Override
	public double calcularPerimetro() {
		double soma = 0;
		for (double tamanho : tamanhos)
			soma += tamanho;
		return soma;
	}
}
