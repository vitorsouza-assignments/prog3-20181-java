package prova01;

public class Questao03 {

	public static void main(String[] args) {
		Forma[] formas = new Forma[] { new Retangulo(new Ponto(1, 1), new Ponto(1, 11), new Ponto(6, 11), new Ponto(6, 1)), new Circulo(new Ponto(6, 11), 5) };
		for (Forma f : formas)
			System.out.println(f);
	}

}
