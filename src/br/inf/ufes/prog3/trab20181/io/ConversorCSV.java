package br.inf.ufes.prog3.trab20181.io;

import java.text.ParseException;
import java.util.List;

/**
 * TODO: document this type.
 * 
 * Vitor E. Silva Souza (vitorsouza@gmail.com)
 */
public interface ConversorCSV<T> {
	/**
	 * TODO: document this method.
	 * 
	 * @param dados
	 * @param lista
	 * @throws ParseException
	 */
	void criarObjetoDeLinhaCSV(String[] dados, List<T> lista) throws ParseException;
}
