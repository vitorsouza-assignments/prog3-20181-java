package br.inf.ufes.prog3.trab20181.io;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import br.inf.ufes.prog3.trab20181.dominio.Curso;
import br.inf.ufes.prog3.trab20181.dominio.Discente;
import br.inf.ufes.prog3.trab20181.dominio.Disciplina;
import br.inf.ufes.prog3.trab20181.dominio.Docente;
import br.inf.ufes.prog3.trab20181.dominio.NivelCurso;
import br.inf.ufes.prog3.trab20181.dominio.Orientacao;
import br.inf.ufes.prog3.trab20181.dominio.OrientacaoGraduacao;
import br.inf.ufes.prog3.trab20181.dominio.OrientacaoPos;
import br.inf.ufes.prog3.trab20181.dominio.ProducaoCientifica;

/**
 * Classe responsável pela leitura dos dados das planilhas.
 *
 * @author Vítor E. Silva Souza (vitorsouza@gmail.com)
 * @version 1.0
 */
public class Leitor {
	/** Formatador de datas. */
	static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

	/** Docentes cadastrados, indexados por seus respectivos códigos. */
	private Map<Long, Docente> docentes = new HashMap<>();

	/** Discentes cadastrados, indexados por suas respectivas matrículas. */
	private Map<Long, Discente> discentes = new HashMap<>();

	/** Cursos cadastrados, indexados por seus respectivos códigos. */
	private Map<Long, Curso> cursos = new HashMap<>();

	/** Disciplinas cadastradas, indexadas por seus respectivos códigos. */
	private Map<String, Disciplina> disciplinas = new HashMap<>();

	/** Atividades de orientação cadastradas */
	private Set<Orientacao> orientacoes = new HashSet<>();

	/** Publicações cadastradas. */
	private Set<ProducaoCientifica> producao_cientifica = new HashSet<>();

	/** Construtor. */
	public Leitor(File nomeArquivoDocentes, File nomeArquivoDiscentes, File nomeArquivoProducoes, File nomeArquivoCursos, File nomeArquivoDisciplinas, File nomeArquivoOrientacoesGraduacao, File nomeArquivoOrientacoesPosGraduacao) throws IOException, ParseException {
		// Lê os arquivos de dados na ordem correta.
		lerCursos(nomeArquivoCursos);
		lerDocentes(nomeArquivoDocentes);
		lerDisciplinas(nomeArquivoDisciplinas);
		lerDiscentes(nomeArquivoDiscentes);
		lerProducoes(nomeArquivoProducoes);
		lerOrientacoes(nomeArquivoOrientacoesGraduacao);
		lerOrientacoes(nomeArquivoOrientacoesPosGraduacao);
	}

	/**
	 * TODO: document this method.
	 * 
	 * @return
	 */
	public List<Docente> getDocentes() {
		return new ArrayList<Docente>(docentes.values());
	}

	public List<Discente> getDiscentes() {
		return new ArrayList<Discente>(discentes.values());
	}

	public int getQtdDocentes() {
		return docentes.size();
	}

	public int getQtdDiscentes() {
		return discentes.size();
	}

	public int getQtdProducoes() {
		return producao_cientifica.size();
	}

	public int getQtdCursos() {
		return cursos.size();
	}

	public int getQtdAulas() {
		return disciplinas.size();
	}

	public int getQtdOrientacoes() {
		return orientacoes.size();
	}

	/* ================== MÉTODOS DE LEITURA. ================== */

	/**
	 * Lê o cadastro de docentes.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados do cadastro de docentes.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerDocentes(File arquivo) throws IOException, ParseException {
		// Obtém a lista de docentes do arquivo.
		List<Docente> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_DOCENTE, lista);

		// Monta o mapa de docentes por código.
		for (Docente objeto : lista) {
			// Verifica inconsistência: dois docentes com mesmo código.
			if (docentes.containsKey(objeto.getCodigo())) throw new IllegalArgumentException("Código repetido para docente: " + objeto.getCodigo() + ".");
			docentes.put(objeto.getCodigo(), objeto);
		}
	}

	/**
	 * Lê o cadastro de discentes.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados do cadastro de discentes.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerDiscentes(File arquivo) throws IOException, ParseException {
		// Obtém a lista de discentes do arquivo.
		List<Discente> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_DISCENTE, lista);

		// Monta o mapa de disciplinas por código.
		for (Discente objeto : lista) {
			// Verifica inconsistência: dois discentes com o mesmo código.
			if (discentes.containsKey(objeto.getMatricula())) throw new IllegalArgumentException("Código repetido para discente: " + objeto.getMatricula() + ".");
			discentes.put(objeto.getMatricula(), objeto);
		}
	}

	/**
	 * Lê o cadastro de producoes científicas.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados do cadastro de publicações.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerProducoes(File arquivo) throws IOException, ParseException {
		// Obtém a lista de publicações do arquivo.
		List<ProducaoCientifica> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_PRODUCAO, lista);

		// Adiciona as publicações no conjunto.
		producao_cientifica.addAll(lista);
	}

	/**
	 * Lê o cadastro de cursos.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados do cadastro de cursos.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerCursos(File arquivo) throws IOException, ParseException {
		// Obtém a lista de cursos do arquivo.
		List<Curso> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_CURSO, lista);

		// Monta o mapa de cursos por código.
		for (Curso objeto : lista) {
			// Verifica inconsistência: dois cursos com mesmo codigo.
			if (cursos.containsKey(objeto.getCodigo())) throw new IllegalArgumentException("Código repetido para curso: " + objeto.getCodigo() + ".");
			cursos.put(objeto.getCodigo(), objeto);
		}
	}

	/**
	 * Lê o cadastro de disciplinas.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados do cadastro de disciplinas.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerDisciplinas(File arquivo) throws IOException, ParseException {
		// Obtém a lista de disciplinas do arquivo.
		List<Disciplina> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_DISCIPLINA, lista);

		// Monta o mapa de disciplinas por código.
		for (Disciplina objeto : lista) {
			// Verifica inconsistência: duas disciplinas com mesmo codigo.
			if (disciplinas.containsKey(objeto.getCodigo())) throw new IllegalArgumentException("Código repetido para disciplina: " + objeto.getCodigo() + ".");
			disciplinas.put(objeto.getCodigo(), objeto);
		}
	}

	/**
	 * Lê a lista de orientações.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados das orientações.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerOrientacoes(File arquivo) throws IOException, ParseException {
		// Obtém a lista de orientacoes do arquivo.
		List<Orientacao> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_ORIENTACAO, lista);

		// Adiciona as orientacoes no conjunto.
		orientacoes.addAll(lista);
	}

	/**
	 * Método genérico para leitura de arquivos CSV. Os dados lidos do arquivo são convertidos para o objeto específico
	 * por meio de um conversor e colocados em uma lista.
	 * 
	 * @param arquivo
	 *          Arquivo que contém os dados em formato CSV.
	 * @param conversor
	 *          Conversor responsável por criar um objeto a partir de uma linha CSV.
	 * @param lista
	 *          Lista onde os objetos criados serão colocados.
	 * @throws IOException
	 *           No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException
	 *           No caso de erros de formatação dos dados do arquivo.
	 */
	private <T> void lerArquivo(File arquivo, ConversorCSV<T> conversor, List<T> lista) throws IOException, ParseException {
		// Cria um scanner para ler o arquivo linha por linha.
		try (Scanner scanner = new Scanner(arquivo)) {
			// Despreza a primeira linha (título) e lê as demais.
			if (scanner.hasNextLine()) scanner.nextLine();
			while (scanner.hasNextLine()) {
				String linha = scanner.nextLine();
				if ((linha != null) && (!linha.isEmpty())) {
					// Separa os dados conditos na linha pelos ponto-e-vírgula usados como separadores.
					String[] dados = linha.split(";");

					// Remove espaços que estejam sobrando nas strings.
					for (int i = 0; i < dados.length; i++)
						if (dados[i] != null) dados[i] = dados[i].trim();

					// Cria um novo objeto a partir do conversor e adiciona-o à lista.
					conversor.criarObjetoDeLinhaCSV(dados, lista);
				}
			}
		}
	}

	/* ================== CONVERSORES CSV. ================== */

	/** Conversor CSV para docentes. */
	private final ConversorCSV<Docente> CONVERSOR_CSV_DOCENTE = new ConversorCSV<Docente>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Docente> lista) throws ParseException {
			// Cria o docente e adiciona à lista.
			Docente docente = new Docente(Long.valueOf(dados[0]), dados[1], dados[2]);
			lista.add(docente);
		}
	};

	/** Conversor CSV para veículos. */
	private final ConversorCSV<Discente> CONVERSOR_CSV_DISCENTE = new ConversorCSV<Discente>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Discente> lista) throws ParseException {
			Curso curso = cursos.get(Long.valueOf(dados[2]));
			if (curso == null) throw new IllegalArgumentException("Código do curso não definido \"" + dados[2] + "\": " + dados[1] + ".");

			// Cria o discente e adiciona à lista
			Discente discente = new Discente(Long.valueOf(dados[0]), dados[1], curso);
			lista.add(discente);
		}
	};

	/** Conversor CSV para publicações. */
	private final ConversorCSV<ProducaoCientifica> CONVERSOR_CSV_PRODUCAO = new ConversorCSV<ProducaoCientifica>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<ProducaoCientifica> lista) throws ParseException {
			// Obtém o docente a partir da sigla. Verifica inconsistência: docente não existe.
			Docente docente = docentes.get(Long.valueOf(dados[0]));
			if (docente == null) throw new IllegalArgumentException("Código do docente inválido na publicação \"" + dados[1] + "\": " + dados[0] + ".");

			boolean qualificada;
			if (dados.length == 3 && dados[2].equals("X")) {
				qualificada = true;
			}
			else {
				qualificada = false;
			}

			ProducaoCientifica producao = new ProducaoCientifica(dados[1], qualificada);

			// Associa a publicação aos docente.
			docente.addProducao(producao);

			// Adiciona a publicação à lista.
			lista.add(producao);
		}
	};

	/** Conversor CSV para cursos. */
	private final ConversorCSV<Curso> CONVERSOR_CSV_CURSO = new ConversorCSV<Curso>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Curso> lista) throws ParseException {
			// Determina o Nivel do Curso
			NivelCurso nivel = null;
			if (dados.length > 2 && dados[2].equals("X")) {
				nivel = NivelCurso.GRADUACAO;

			}
			else if (dados.length > 3 && dados[3].equals("X")) {
				nivel = NivelCurso.POSGRADUACAO;
			}
			else {
				throw new IllegalArgumentException("Inconsistência ao definir o nível do curso: " + dados[0] + " - " + dados[1] + ".");
			}

			// Cria a qualificação e adiciona à lista.
			Curso curso = new Curso(Long.valueOf(dados[0]), dados[1], nivel);
			lista.add(curso);
		}
	};

	/** Conversor CSV para disciplinas. */
	private final ConversorCSV<Disciplina> CONVERSOR_CSV_DISCIPLINA = new ConversorCSV<Disciplina>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Disciplina> lista) throws ParseException {
			// Obtém o curso a partir da sigla. Verifica inconsistência: curso não existe.
			Curso curso = cursos.get(Long.valueOf(dados[5]));
			if (curso == null) throw new IllegalArgumentException("Código do curso inválido na disciplina \"" + dados[1] + "\": " + dados[5] + ".");

			// Obtém o docente a partir da sigla. Verifica inconsistência: docente não existe.
			Docente docente = docentes.get(Long.valueOf(dados[2]));
			if (docente == null) throw new IllegalArgumentException("Código do docente inválido na disciplina \"" + dados[1] + "\": " + dados[2] + ".");

			Disciplina disciplina = new Disciplina(dados[0], dados[1], Integer.valueOf(dados[3]), Integer.valueOf(dados[4]), curso);

			// Adiciona a lista de disciplinas do docente
			docente.addDisciplina(disciplina);

			// Adiciona a lista de disciplinas do curso
			curso.addDisciplina(disciplina);

			// Adiciona a disciplina à lista.
			lista.add(disciplina);
		}
	};

	private boolean isFuture(String pDateString) throws ParseException {
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(pDateString);
		return new Date().before(date);
	}

	/** Conversor CSV para atividades de orientação. */
	private final ConversorCSV<Orientacao> CONVERSOR_CSV_ORIENTACAO = new ConversorCSV<Orientacao>() {
		/**
		 * @see br.ufes.inf.prog3.trab20152.io.ConversorCSV#criarObjetoDeLinhaCSV(java.lang.String[], java.util.List)
		 */
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Orientacao> lista) throws ParseException {
			Discente discente = discentes.get(Long.valueOf(dados[1]));
			if (discente == null) throw new IllegalArgumentException("Código do discente não definido em orientação: \"" + dados[1] + ".");

			Docente docente = docentes.get(Long.valueOf(dados[0]));
			if (docente == null) throw new IllegalArgumentException("Código do docente inválido na orientacão do aluno \"" + discente.getNome() + "\": " + dados[0] + ".");

			Orientacao orientacao = null;

			if (dados.length == 4) {
				// Obtém o curso a partir do codigo. Verifica inconsistência: curso não existe.
				Curso curso = cursos.get(Long.valueOf(dados[2]));
				if (curso == null) throw new IllegalArgumentException("Código do curso inválido na orientação do aluno \"" + discente.getNome() + "\": " + dados[2] + ".");

				// Verifica se o orientado de graduação é de um curso de graduação.
				if (discente.getCurso().getNivelCurso() != NivelCurso.GRADUACAO)
					throw new IllegalArgumentException("Orientação de graduação se refere a aluno de curso que não é de graduação: " + discente.getNome());
				
				orientacao = new OrientacaoGraduacao(discente, curso, Integer.parseInt(dados[3]));

			}
			else if (dados.length == 5) {
				// Verifica inconsistência: data de ingresso no futuro.
				if (isFuture(dados[2])) throw new IllegalArgumentException("Data de ingresso do aluno: \"" + discente.getNome() + "\" está no futuro: " + dados[2]);
				
				// Verifica se o orientado de graduação é de um curso de pós-graduação.
				if (discente.getCurso().getNivelCurso() != NivelCurso.POSGRADUACAO)
					throw new IllegalArgumentException("Orientação de pós-graduação se refere a aluno de curso que não é de pós-graduação: " + discente.getNome());
				
				orientacao = new OrientacaoPos(discente, FORMATA_DATAS.parse(dados[2]), dados[3], Integer.parseInt(dados[4]));
			}

			// Adiciona atividade ao docente
			docente.addOrientacao(orientacao);
			discente.addOrientacao(orientacao);
			lista.add(orientacao);
		}
	};
}
