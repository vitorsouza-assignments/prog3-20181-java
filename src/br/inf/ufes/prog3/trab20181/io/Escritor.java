package br.inf.ufes.prog3.trab20181.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import br.inf.ufes.prog3.trab20181.AplRelatorio;
import br.inf.ufes.prog3.trab20181.dominio.Curso;
import br.inf.ufes.prog3.trab20181.dominio.Discente;
import br.inf.ufes.prog3.trab20181.dominio.Disciplina;
import br.inf.ufes.prog3.trab20181.dominio.Docente;
import br.inf.ufes.prog3.trab20181.dominio.NivelCurso;
import br.inf.ufes.prog3.trab20181.dominio.OrientacaoPos;
import br.inf.ufes.prog3.trab20181.util.ComparadorDisciplina;
import br.inf.ufes.prog3.trab20181.util.ComparadorDocente;
import br.inf.ufes.prog3.trab20181.util.ComparadorPPG;
import br.inf.ufes.prog3.trab20181.util.ComparadorRHACurso;
import br.inf.ufes.prog3.trab20181.util.ComparadorRHADocente;

/**
 * Classe responsável pela escrita dos dados: serialização da aplicação e produção de relatórios.
 *
 * @author Vítor E. Silva Souza (vitorsouza@gmail.com)
 * @version 1.0
 */
public class Escritor {
	/** Separador de colunas CSV. */
	private static final String SEPARADOR_CSV = ";";

	/** Cabeçalho do arquivo de relatório de atividades. */
	private static final String CABECALHO_RELATORIO_PAD = "Docente" + SEPARADOR_CSV + "Departamento" + SEPARADOR_CSV + "Horas Semanais Aula" + SEPARADOR_CSV + "Horas Semestrais Aula" + SEPARADOR_CSV + "Horas Semanais Orientação" + SEPARADOR_CSV + "Produções Qualificadas" + SEPARADOR_CSV + "Produções Não Qualificadas";

	/** Cabeçalho do arquivo de relatório de horas aulas. */
	private static final String CABECALHO_RELATORIO_RHA = "Departamento" + SEPARADOR_CSV + "Docente" + SEPARADOR_CSV + "Cód. Curso" + SEPARADOR_CSV + "Curso" + SEPARADOR_CSV + "Horas Semestrais Aula";

	/** Cabeçalho do arquivo de relatório de alocação. */
	private static final String CABECALHO_RELATORIO_ALOCACAO = "Docente" + SEPARADOR_CSV + "Código" + SEPARADOR_CSV + "Nome" + SEPARADOR_CSV + "Carga Horária Semestral";

	/** Cabeçalho do arquivo de relatório de estudantes do PPG. */
	private static final String CABECALHO_RELATORIO_PPG = "Nome do Programa" + SEPARADOR_CSV + "Data de Ingresso" + SEPARADOR_CSV + "Matrícula" + SEPARADOR_CSV + "Nome";

	/** Arquivo onde a aplicação deve ser (des)serializada. */
	private File arquivoSerializacao;

	/** Arquivo no qual será escrito o relatório PAD. */
	private File arquivoPAD;

	/** Arquivo no qual será escrito o relatório RHA. */
	private File arquivoRHA;

	/** Arquivo no qual será escrito o relatório de alocação de disciplinas. */
	private File arquivoRelatorioAlocacao;

	/** Arquivo no qual será escrito o relatório do PPG. */
	private File arquivoRelatorioPPG;

	/** Construtor. */
	public Escritor(File arquivoSerializacao, File arquivoPAD, File arquivoRHA, File arquivoRelatorioAlocacao, File arquivoRelatorioPPG) {
		this.arquivoSerializacao = arquivoSerializacao;
		this.arquivoPAD = arquivoPAD;
		this.arquivoRelatorioAlocacao = arquivoRelatorioAlocacao;
		this.arquivoRHA = arquivoRHA;
		this.arquivoRelatorioPPG = arquivoRelatorioPPG;
	}

	/**
	 * Escreve o relatório PAD.
	 * 
	 * @param ano
	 * @param regras
	 * @param docentes
	 * @throws IOException
	 *           No caso de erros de entrada e saída na escrita.
	 */
	public void escreverPAD(List<Docente> docentes) throws IOException {
		// Cria um escritor em cima do arquivo.
		try (PrintWriter out = new PrintWriter(arquivoPAD)) {
			// Escreve o título das colunas do CSV.
			out.printf("%s%n", CABECALHO_RELATORIO_PAD);

			ComparadorDocente c = new ComparadorDocente(docentes);
			Collections.sort(docentes, c);

			// Processa todos os docentes do programa gerando o PAD a partir deles.
			for (Docente docente : docentes) {
				// Imprime as informações.
				out.printf("%s%s%s%s%d%s%d%s%d%s%d%s%d%n", docente.getNome(), SEPARADOR_CSV, docente.getDepartamento(), SEPARADOR_CSV, docente.getHorasSemanaisAulas(), SEPARADOR_CSV, docente.getHorasSemestraisAulas(), SEPARADOR_CSV, docente.getHorasSemanaisOrientacao(), SEPARADOR_CSV, docente.getNumeroProducoesQualificadas(), SEPARADOR_CSV, docente.getNumeroProducoesNaoQualificadas());
			}
		}

	}

	/**
	 * Escreve o relatório RHA.
	 * 
	 * @param publicacoesRelatorio
	 * @throws IOException
	 *           No caso de erros de entrada e saída na escrita.
	 */
	public void escreverRelatorioRHA(List<Docente> docentes) throws IOException {
		// Cria um escritor em cima do arquivo.
		try (PrintWriter out = new PrintWriter(arquivoRHA)) {
			// Escreve o título das colunas do CSV.
			out.printf("%s%n", CABECALHO_RELATORIO_RHA);

			// ordena docentes por departamento e nome do docente
			ComparadorRHADocente c = new ComparadorRHADocente();
			Collections.sort(docentes, c);

			for (Docente docente : docentes) {
				List<Disciplina> disciplinas = docente.getDisciplinas();
				Map<Curso, Integer> cursos = new HashMap<>();

				// Soma as horas relativas a cada disciplina do mesmo curso e gera um mapa
				for (Disciplina disciplina : disciplinas) {
					Curso curso = disciplina.getCurso();

					if (cursos.containsKey(curso)) {
						int horas = cursos.get(curso);
						horas += disciplina.getCargaHorariaSemestral();
						cursos.put(curso, horas);
					}
					else {
						cursos.put(curso, disciplina.getCargaHorariaSemestral());
					}
				}

				// Ordena por nome do curso
				TreeMap<Curso, Integer> curso_ordered = new TreeMap<>(new ComparadorRHACurso());
				curso_ordered.putAll(cursos);
				for (Curso curso : curso_ordered.keySet()) {
					int h = cursos.get(curso);
					out.printf("%s%s%s%s%d%s%s%s%d%n", docente.getDepartamento(), SEPARADOR_CSV, docente.getNome(), SEPARADOR_CSV, curso.getCodigo(), SEPARADOR_CSV, curso.getNome(), SEPARADOR_CSV, h);
				}
			}
		}
	}

	/**
	 * Escreve o relatório de alocação.
	 * 
	 * @param estatisticas
	 * @throws IOException
	 *           No caso de erros de entrada e saída na escrita.
	 */
	public void escreverRelatorioAlocacao(List<Docente> docentes) throws IOException {
		// Cria um escritor em cima do arquivo.
		try (PrintWriter out = new PrintWriter(arquivoRelatorioAlocacao)) {
			// Escreve o título das colunas do CSV.
			out.printf("%s%n", CABECALHO_RELATORIO_ALOCACAO);

			// Ordena o nome dos docentes
			ComparadorDocente c = new ComparadorDocente(docentes);
			Collections.sort(docentes, c);

			// Processa todos os docentes do programa gerando o relatorio de alocacao a partir deles.
			for (Docente docente : docentes) {
				List<Disciplina> disciplinas = docente.getDisciplinas();
				ComparadorDisciplina d = new ComparadorDisciplina(disciplinas);
				Collections.sort(disciplinas, d);

				for (Disciplina disciplina : disciplinas) {
					// Imprime as informações.
					out.printf("%s%s%s%s%s%s%d%n", docente.getNome(), SEPARADOR_CSV, disciplina.getCodigo(), SEPARADOR_CSV, disciplina.getNome(), SEPARADOR_CSV, disciplina.getCargaHorariaSemestral());
				}
			}
		}
	}

	/**
	 * Escreve o relatório do PPG.
	 * 
	 * @param estatisticas
	 * @throws IOException
	 *           No caso de erros de entrada e saída na escrita.
	 */
	public void escreverRelatorioPPG(List<Discente> discentes) throws IOException {
		// Cria um escritor em cima do arquivo.
		try (PrintWriter out = new PrintWriter(arquivoRelatorioPPG)) {
			// Escreve o título das colunas do CSV.
			out.printf("%s%n", CABECALHO_RELATORIO_PPG);

			// Cria lista de discentes que somente são PPGfor
			List<Discente> discentes_ppg = new ArrayList<>();
			for (Discente discente : discentes) {
				if (discente.getCurso().getNivelCurso() == NivelCurso.POSGRADUACAO) {
					discentes_ppg.add(discente);
				}
			}

			// Ordena a lista de discentes do PPG
			ComparadorPPG c = new ComparadorPPG();
			Collections.sort(discentes_ppg, c);

			// Formatador de datas
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

			// Escreve o relatorio.
			for (Discente discente : discentes_ppg) {
				OrientacaoPos o = (OrientacaoPos) discente.getOrientacao();
				if (o != null) out.printf("%s%s%s%s%d%s%s%n", o.getNomePPG(), SEPARADOR_CSV, formato.format(o.getDataIngresso()), SEPARADOR_CSV, discente.getMatricula(), SEPARADOR_CSV, discente.getNome());
			}
		}
	}

	/**
	 * Serializa a aplicação e todos os dados já calculados.
	 * 
	 * @param aplicacao
	 *          Aplicação a ser serializada.
	 * @throws IOException
	 *           No caso de erros de entrada e saída na escrita.
	 */
	public void serializar(AplRelatorio aplicacao) throws IOException {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(arquivoSerializacao))) {
			out.writeObject(aplicacao);
		}
	}

	/**
	 * Desserializa a aplicação e todos os dados já calculados.
	 * 
	 * @return A aplicação restaurada.
	 * @throws IOException
	 *           No caso de erros de entrada e saída na leitura.
	 * @throws ClassNotFoundException
	 *           No caso de ler alguma classe no arquivo que não se encontra no classpath.
	 */
	public AplRelatorio desserializar() throws IOException, ClassNotFoundException {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(arquivoSerializacao))) {
			return (AplRelatorio) in.readObject();
		}
	}
}
