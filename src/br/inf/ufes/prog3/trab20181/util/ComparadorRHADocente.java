package br.inf.ufes.prog3.trab20181.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Docente;

public class ComparadorRHADocente implements Comparator<Docente> {
	Locale locale = new Locale("pt", "BR");
	Collator collator = Collator.getInstance(locale);

	@Override
	public int compare(Docente left, Docente right) {
		if (left.getDepartamento().equals(right.getDepartamento())) {
			return collator.compare(left.getNome(), right.getNome());
		}
		else {
			return collator.compare(left.getDepartamento(), right.getDepartamento());
		}
	}
}
