package br.inf.ufes.prog3.trab20181.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Docente;

public class ComparadorDocente implements Comparator<Docente> {
	Locale locale = new Locale("pt", "BR");
	Collator collator = Collator.getInstance(locale);
	List<Docente> base;

	public ComparadorDocente(List<Docente> base) {
		this.base = base;
	}

	public int compare(Docente a, Docente b) {
		return collator.compare(a.getNome(), b.getNome());
	}
}
