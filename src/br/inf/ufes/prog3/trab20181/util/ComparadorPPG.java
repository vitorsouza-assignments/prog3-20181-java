package br.inf.ufes.prog3.trab20181.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Discente;
import br.inf.ufes.prog3.trab20181.dominio.OrientacaoPos;

public class ComparadorPPG implements Comparator<Discente> {
	Locale locale = new Locale("pt", "BR");
	Collator collator = Collator.getInstance(locale);

	public int compare(Discente a, Discente b) {
		OrientacaoPos o1 = (OrientacaoPos) a.getOrientacao();
		OrientacaoPos o2 = (OrientacaoPos) b.getOrientacao();

		if (collator.compare(o1.getNomePPG(), o2.getNomePPG()) == 0) {
			Date d1 = o1.getDataIngresso();
			Date d2 = o2.getDataIngresso();
			if (d1.equals(d2)) {
				return collator.compare(a.getNome(), b.getNome());
			}
			else {
				return d1.compareTo(d2);
			}
		}
		else {
			return collator.compare(o1.getNomePPG(), o2.getNomePPG());
		}
	}
}
