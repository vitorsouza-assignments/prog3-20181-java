package br.inf.ufes.prog3.trab20181.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Disciplina;

public class ComparadorDisciplina implements Comparator<Disciplina> {
	Locale locale = new Locale("pt", "BR");
	Collator collator = Collator.getInstance(locale);
	List<Disciplina> base;

	public ComparadorDisciplina(List<Disciplina> base) {
		this.base = base;
	}

	public int compare(Disciplina a, Disciplina b) {
		return collator.compare(a.getCodigo(), b.getCodigo());
	}
}
