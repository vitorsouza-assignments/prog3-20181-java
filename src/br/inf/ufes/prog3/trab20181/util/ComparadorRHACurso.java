package br.inf.ufes.prog3.trab20181.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Curso;

public class ComparadorRHACurso implements Comparator<Curso> {
	Locale locale = new Locale("pt", "BR");
	Collator collator = Collator.getInstance(locale);

	@Override
	public int compare(Curso left, Curso right) {
		return collator.compare(left.getNome(), right.getNome());
	}
}
