package br.inf.ufes.prog3.trab20181;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.dominio.Discente;
import br.inf.ufes.prog3.trab20181.dominio.Docente;
import br.inf.ufes.prog3.trab20181.io.Escritor;
import br.inf.ufes.prog3.trab20181.io.Leitor;

/**
 * Classe principal do trabalho de Prog3 2018/1.
 * 
 * Contém o método <code>main()</code> para execuçío, além de métodos auxiliares para processamento dos arquivos de
 * entrada e geraçío dos arquivos de saída.
 *
 * @author Ví­tor E. Silva Souza (vitorsouza@gmail.com)
 * @version 1.0
 */
public class AplRelatorio implements Serializable {
	/** ID de serializaçío. */
	private static final long serialVersionUID = 1L;

	/** Nome do arquivo no qual o estado da aplicaçío é serializado. */
	private static final String ARQUIVO_SERIALIZACAO = "dados.dat";

	/** Nome do arquivo de relatório do PAD. */
	private static final String RELATORIO_PAD = "1-pad.csv";

	/** Nome do arquivo de relatório de horas-aula. */
	private static final String RELATORIO_RHA = "2-rha.csv";

	/** Nome do arquivo de relatório de alocaçío de disciplinas. */
	private static final String RELATORIO_ALOCACAO = "3-alocacao.csv";

	/** Nome do arquivo de relatório de discentes da pós. */
	private static final String RELATORIO_PPG = "4-ppg.csv";

	/** Indica se o programa deve serializar os dados ao invés de gerar as saí­das. */
	private boolean serializar;

	/** TODO: document this field. */
	private List<Docente> docentes;

	/** TODO: document this field. */
	private List<Discente> discentes;

	/** Método principal. Cria uma instância da aplicaçío e executa. */
	public static void main(String[] args) {
		AplRelatorio apl = null;
		boolean readOnly = false, writeOnly = false;
		String nomeArquivoDocentes = null, nomeArquivoDiscente = null, nomeArquivoProducoes = null, nomeArquivoCursos = null, nomeArquivoDisciplinas = null, nomeArquivoOrientacaoGraduacao = null, nomeArquivoOrientacaoPos = null;

		// Determina configurações regionais.
		Locale.setDefault(new Locale("pt", "BR"));

		try {
			// Processa os parâmetros da linha de comando.
			for (int i = 0; i < args.length; i++) {
				// Procura pela opção -d, que especifica o arquivo de docentes.
				if ("-d".equals(args[i]) && args.length > i + 1) nomeArquivoDocentes = args[i + 1];

				// Procura pela opção -a, que especifica o arquivo de discentes.
				else if ("-a".equals(args[i]) && args.length > i + 1) nomeArquivoDiscente = args[i + 1];

				// Procura pela opção -p, que especifica o arquivo de produções.
				else if ("-p".equals(args[i]) && args.length > i + 1) nomeArquivoProducoes = args[i + 1];

				// Procura pela opção -c, que especifica o arquivo de cursos.
				else if ("-c".equals(args[i]) && args.length > i + 1) nomeArquivoCursos = args[i + 1];

				// Procura pela opção -r, que especifica o arquivo de aulas.
				else if ("-r".equals(args[i]) && args.length > i + 1) nomeArquivoDisciplinas = args[i + 1];

				// Procura pela opção -og, que especifica o arquivo de orientações na graduação.
				else if ("-og".equals(args[i]) && args.length > i + 1) nomeArquivoOrientacaoGraduacao = args[i + 1];

				// Procura pela opção -op, que especifica o arquivo de orientações na pós.
				else if ("-op".equals(args[i]) && args.length > i + 1) nomeArquivoOrientacaoPos = args[i + 1];

				// Procura pela opções --read-only e --write-only, que indicam o uso de serialização.
				else if ("--read-only".equals(args[i])) readOnly = true;
				else if ("--write-only".equals(args[i])) writeOnly = true;
			}

			// Cria o escritor.
			Escritor escritor = new Escritor(new File(ARQUIVO_SERIALIZACAO), new File(RELATORIO_PAD), new File(RELATORIO_RHA), new File(RELATORIO_ALOCACAO), new File(RELATORIO_PPG));

			// Se os nomes dos arquivos não foram especificados, imprime mensagem de erro.
			if (!writeOnly && (nomeArquivoDocentes == null || nomeArquivoDiscente == null || nomeArquivoProducoes == null || nomeArquivoCursos == null || nomeArquivoDisciplinas == null || nomeArquivoOrientacaoGraduacao == null || nomeArquivoOrientacaoPos == null)) System.out.printf("Arquivos de entrada não especificados. Use: -d <arquivo> -a <arquivo> -p <arquivo> -c <arquivo> -r <arquivo> -og <arquivo> -op <arquivo>%n");

			// Do contrário, executa a aplicação. Verifica primeiro se devemos restaurá-la por serialização.
			else if (writeOnly) {
				apl = escritor.desserializar();
				apl.serializar = false;
			}

			// Se não é pra restaurar a aplicação serializada, cria uma nova aplicação e lê os dados dos arquivos.
			else {
				Leitor leitor = new Leitor(new File(nomeArquivoDocentes), new File(nomeArquivoDiscente), new File(nomeArquivoProducoes), new File(nomeArquivoCursos), new File(nomeArquivoDisciplinas), new File(nomeArquivoOrientacaoGraduacao), new File(nomeArquivoOrientacaoPos));
				apl = new AplRelatorio(leitor, readOnly);
				// apl.processarDados();
			}

			// Finalmente, executa a aplicação.
			if (apl != null) apl.executar(escritor);
		}
		catch (IOException | ClassNotFoundException e) {
			System.out.println("Erro de I/O");
			//e.printStackTrace();
		}
		catch (ParseException | NumberFormatException e) {
			System.out.println("Erro de formatação");
			//e.printStackTrace();
		}
		catch (IllegalArgumentException e) {
			System.out.printf("%s%n", e.getMessage());
			//e.printStackTrace();
		}
	}

	/** Construtor. */
	public AplRelatorio(Leitor leitor, boolean readOnly) {
		this.serializar = readOnly;

		// Extrai do leitor as coleções necessárias.
		docentes = leitor.getDocentes();
		discentes = leitor.getDiscentes();
	}

	/**
	 * Produz a saí­da, seja serializando a aplicação (read-only) ou escrevendo os relatórios.
	 * 
	 * @param escritor
	 *          Objeto responsável pela escrita dos relatórios.
	 * @throws IOException
	 *           No caso de erros de escrita (acesso aos arquivos de relatório).
	 */
	private void executar(Escritor escritor) throws IOException {
		// Verifica se é pra serializar ou pra produzir os relatórios.
		if (serializar) escritor.serializar(this);
		else {
			// Escreve os relatórios.
			escritor.escreverPAD(docentes);
			escritor.escreverRelatorioRHA(docentes);
			escritor.escreverRelatorioAlocacao(docentes);
			escritor.escreverRelatorioPPG(discentes);
		}
	}
}
