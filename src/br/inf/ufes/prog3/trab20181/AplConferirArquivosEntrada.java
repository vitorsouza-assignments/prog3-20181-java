package br.inf.ufes.prog3.trab20181;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Locale;

import br.inf.ufes.prog3.trab20181.io.Leitor;

/**
 * Aplicativo para verificar se os arquivos de testes produzidos pelos alunos passam o critério mínimo para ganhar 1
 * ponto extra.
 * 
 * @author Vitor E. Silva Souza (vitorsouza@gmail.com)
 */
public class AplConferirArquivosEntrada implements Serializable {
	/** Serialization ID. */
	private static final long serialVersionUID = 1L;

	/** Nome do arquivo que possui os dados de aulas. */
	private static final String NOME_ARQUIVO_AULAS = "aulas.csv";

	/** Nome do arquivo que possui os dados de cursos. */
	private static final String NOME_ARQUIVO_CURSOS = "cursos.csv";

	/** Nome do arquivo que possui os dados de alunos. */
	private static final String NOME_ARQUIVO_DISCENTES = "discentes.csv";

	/** Nome do arquivo que possui os dados de professores. */
	private static final String NOME_ARQUIVO_DOCENTES = "docentes.csv";

	/** Nome do arquivo que possui os dados de orientações na graduação. */
	private static final String NOME_ARQUIVO_ORIENTAGRAD = "orientagrad.csv";

	/** Nome do arquivo que possui os dados de orientações na pós-graduação. */
	private static final String NOME_ARQUIVO_ORIENTAPOS = "orientapos.csv";

	/** Nome do arquivo que possui os dados de produções científicas. */
	private static final String NOME_ARQUIVO_PRODUCOES = "producoes.csv";

	/** Número mínimo de docentes nos dados fornecidos. */
	private static final int MINIMO_DOCENTES = 5;

	/** Número mínimo de discentes nos dados fornecidos. */
	private static final int MINIMO_DISCENTES = 30;

	/** Número mínimo de produções nos dados fornecidos. */
	private static final int MINIMO_PRODUCOES = 30;

	/** Número mínimo de cursos nos dados fornecidos. */
	private static final int MINIMO_CURSOS = 15;

	/** Número mínimo de aulas nos dados fornecidos. */
	private static final int MINIMO_AULAS = 30;

	/** Número mínimo de atividades de orientação nos dados fornecidos. */
	private static final int MINIMO_ORIENTACOES = 20;

	/** Leitor de dados. */
	private Leitor leitor;

	/** Método principal. Cria uma instância da aplicação e executa. */
	public static void main(String[] args) throws ParseException, ClassNotFoundException {
		try {
			Locale.setDefault(new Locale("pt", "BR"));
			AplConferirArquivosEntrada apl = new AplConferirArquivosEntrada();
			apl.lerPlanilhas(NOME_ARQUIVO_AULAS, NOME_ARQUIVO_CURSOS, NOME_ARQUIVO_DISCENTES, NOME_ARQUIVO_DOCENTES, NOME_ARQUIVO_ORIENTAGRAD, NOME_ARQUIVO_ORIENTAPOS, NOME_ARQUIVO_PRODUCOES);
			apl.executar();
		}
		catch (IOException e) {
			System.out.printf("Erro de I/O%n");
			e.printStackTrace();
		}
		catch (ParseException | NumberFormatException e) {
			System.out.printf("Erro de formatação%n");
			e.printStackTrace();
		}
		catch (IllegalArgumentException e) {
			System.out.printf("Dados inconsistentes (%s)%n", e.getMessage());
			e.printStackTrace();
		}
	}

	/** Lê as planilhas de entrada de dados especificadas. */
	private void lerPlanilhas(String nomeArquivoAulas, String nomeArquivoCursos, String nomeArquivoDiscentes, String nomeArquivoDocentes, String nomeArquivoOrientaGrad, String nomeArquivoOrientaPos, String nomeArquivoProducoes) throws IOException, ParseException {
		leitor = new Leitor(new File(nomeArquivoDocentes), new File(nomeArquivoDiscentes), new File(nomeArquivoProducoes), new File(nomeArquivoCursos), new File(nomeArquivoAulas), new File(nomeArquivoOrientaGrad), new File(nomeArquivoOrientaPos));
	}

	/**
	 * Executa a aplicação, ou seja, efetua os cálculos e salva-os seja no arquivo serializado (--read-only) ou nos
	 * relatórios de saída (execução normal).
	 * 
	 * @throws IOException
	 */
	private void executar() throws IOException {
		// Verifica os números absolutos de cada dado.
		int qtdDocentes = leitor.getQtdDocentes();
		int qtdDiscentes = leitor.getQtdDiscentes();
		int qtdProducoes = leitor.getQtdProducoes();
		int qtdCursos = leitor.getQtdCursos();
		int qtdAulas = leitor.getQtdAulas();
		int qtdOrientacoes = leitor.getQtdOrientacoes();

		System.out.printf("Docentes: %d\t(%s)%n", qtdDocentes, (qtdDocentes >= MINIMO_DOCENTES ? "OK" : "NOT!!!"));
		System.out.printf("Discentes: %d\t(%s)%n", qtdDiscentes, (qtdDiscentes >= MINIMO_DISCENTES ? "OK" : "NOT!!!"));
		System.out.printf("Produções: %d\t(%s)%n", qtdProducoes, (qtdProducoes >= MINIMO_PRODUCOES ? "OK" : "NOT!!!"));
		System.out.printf("Cursos: %d\t(%s)%n", qtdCursos, (qtdCursos >= MINIMO_CURSOS ? "OK" : "NOT!!!"));
		System.out.printf("Aulas: %d\t(%s)%n", qtdAulas, (qtdAulas >= MINIMO_AULAS ? "OK" : "NOT!!!"));
		System.out.printf("Orientações: %d\t(%s)%n", qtdOrientacoes, (qtdOrientacoes >= MINIMO_ORIENTACOES ? "OK" : "NOT!!!"));

		System.out.println("\nDone!");
	}
}
