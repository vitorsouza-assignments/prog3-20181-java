package br.inf.ufes.prog3.trab20181.dominio;

import java.io.Serializable;

public class Atividade implements Serializable {

	private int cargaHorariaSemanal;
	private int cargaHorariaSemestral;

	public Atividade(int cargaHorariaSemanal, int cargaHorariaSemestral) {
		this.cargaHorariaSemanal = cargaHorariaSemanal;
		this.cargaHorariaSemestral = cargaHorariaSemestral;
	}

	public int getCargaHorariaSemanal() {
		return cargaHorariaSemanal;
	}

	public int getCargaHorariaSemestral() {
		return cargaHorariaSemestral;
	}
}
