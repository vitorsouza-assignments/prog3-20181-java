package br.inf.ufes.prog3.trab20181.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Curso implements Serializable {
	private long codigo;
	private String nome;
	private NivelCurso nivelCurso;

	private List<Disciplina> disciplinas = new ArrayList<>();

	public Curso(long codigo, String nome, NivelCurso nivelCurso) {
		this.codigo = codigo;
		this.nome = nome;
		this.nivelCurso = nivelCurso;
	}

	public long getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public NivelCurso getNivelCurso() {
		return nivelCurso;
	}

	public void addDisciplina(Disciplina d) {
		this.disciplinas.add(d);
	}
}
