package br.inf.ufes.prog3.trab20181.dominio;

import java.io.Serializable;

public class ProducaoCientifica implements Serializable {
	private String titulo;
	private boolean qualificada;

	public ProducaoCientifica(String titulo, boolean qualificada) {
		this.titulo = titulo;
		this.qualificada = qualificada;
	}

	public String getTitulo() {
		return titulo;
	}

	public boolean isQualificada() {
		return qualificada;
	}
}
