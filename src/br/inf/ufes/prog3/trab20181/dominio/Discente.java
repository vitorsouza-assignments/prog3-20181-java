package br.inf.ufes.prog3.trab20181.dominio;

import java.io.Serializable;

public class Discente implements Serializable {
	private long matricula;
	private String nome;
	private Curso curso;
	private Orientacao orientacao;

	public Discente(long matricula, String nome, Curso curso) {
		this.matricula = matricula;
		this.nome = nome;
		this.curso = curso;
	}

	public long getMatricula() {
		return this.matricula;
	}

	public String getNome() {
		return this.nome;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void addOrientacao(Orientacao orientacao) {
		this.orientacao = orientacao;
	}

	public Orientacao getOrientacao() {
		return this.orientacao;
	}

}
