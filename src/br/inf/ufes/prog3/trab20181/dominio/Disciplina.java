package br.inf.ufes.prog3.trab20181.dominio;

public class Disciplina extends Atividade {
	private String codigo;
	private String nome;
	private Curso curso;

	public Disciplina(String codigo, String nome, int cargaHorariaSemanal, int cargaHorariaSemestral, Curso curso) {
		super(cargaHorariaSemanal, cargaHorariaSemestral);
		this.codigo = codigo;
		this.nome = nome;
		this.curso = curso;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public Curso getCurso() {
		return curso;
	}

}
