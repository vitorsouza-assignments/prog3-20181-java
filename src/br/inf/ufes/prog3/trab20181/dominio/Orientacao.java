package br.inf.ufes.prog3.trab20181.dominio;

public class Orientacao extends Atividade {

	private Discente discente;

	public Orientacao(Discente discente, int cargaHorariaSemanal, int cargaHorariaSemestral) {
		super(cargaHorariaSemanal, cargaHorariaSemestral);
		this.discente = discente;
	}

	public Discente getDiscente() {
		return this.discente;
	}
}
