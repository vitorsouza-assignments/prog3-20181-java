package br.inf.ufes.prog3.trab20181.dominio;

public class OrientacaoGraduacao extends Orientacao {

	private Curso curso;

	public OrientacaoGraduacao(Discente discente, Curso curso, int cargaHorariaSemanal) {
		super(discente, cargaHorariaSemanal, 0);
		this.curso = curso;
	}

	public Curso getCurso() {
		return this.curso;
	}
}
