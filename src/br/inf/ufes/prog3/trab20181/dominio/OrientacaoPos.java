package br.inf.ufes.prog3.trab20181.dominio;

import java.util.Date;

public class OrientacaoPos extends Orientacao {
	private String nomePPG;
	private Date dataIngresso;

	public OrientacaoPos(Discente discente, Date dataIngresso, String nomePPG, int cargaHorariaSemanal) {
		super(discente, cargaHorariaSemanal, 0);
		this.nomePPG = nomePPG;
		this.dataIngresso = dataIngresso;
	}

	public String getNomePPG() {
		return this.nomePPG;
	}

	public Date getDataIngresso() {
		return this.dataIngresso;
	}
}
