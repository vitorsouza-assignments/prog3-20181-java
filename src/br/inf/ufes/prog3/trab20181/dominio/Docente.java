package br.inf.ufes.prog3.trab20181.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Docente implements Serializable {
	private long codigo;
	private String nome;
	private String departamento;

	private List<ProducaoCientifica> producoesQualificadas = new ArrayList<>();
	private List<ProducaoCientifica> producoesNaoQualificadas = new ArrayList<>();
	private List<Orientacao> orientacoes = new ArrayList<>();
	private List<Disciplina> disciplinas = new ArrayList<>();

	public Docente(long codigo, String nome, String departamento) {
		this.codigo = codigo;
		this.nome = nome;
		this.departamento = departamento;
	}

	public long getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void addProducao(ProducaoCientifica p) {
		if (p.isQualificada()) {
			this.producoesQualificadas.add(p);
		}
		else {
			this.producoesNaoQualificadas.add(p);
		}
	}

	public void addOrientacao(Orientacao o) {
		this.orientacoes.add(o);
	}

	public void addDisciplina(Disciplina d) {
		this.disciplinas.add(d);
	}

	public int getHorasSemanaisAulas() {
		int horas = 0;

		for (Disciplina d : disciplinas) {
			horas += d.getCargaHorariaSemanal();
		}

		return horas;
	}

	public int getHorasSemestraisAulas() {
		int horas = 0;

		for (Disciplina d : disciplinas) {
			horas += d.getCargaHorariaSemestral();
		}

		return horas;
	}

	public int getHorasSemanaisOrientacao() {
		int horas = 0;

		for (Orientacao o : orientacoes) {
			horas += o.getCargaHorariaSemanal();
		}

		return horas;
	}

	public int getHorasSemestraisOrientacao() {
		int horas = 0;

		for (Orientacao o : orientacoes) {
			horas += o.getCargaHorariaSemestral();
		}

		return horas;
	}

	public int getNumeroProducoesQualificadas() {
		return this.producoesQualificadas.size();
	}

	public int getNumeroProducoesNaoQualificadas() {
		return this.producoesNaoQualificadas.size();
	}

	public List<Disciplina> getDisciplinas() {
		return this.disciplinas;
	}
}
